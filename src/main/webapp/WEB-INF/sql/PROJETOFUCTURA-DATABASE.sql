<!-- comentario para registro do sistema sobre a tabela -->
comment on TABLE usuario IS 'tabela que guarda as informaçoes relacionadas ao usuario';

<!-- tabela usuario com os dados primarios do mesmo-->
CREATE TABLE usuario(
id NUMBER NOT NULL,
nome VARCHAR2(50) NOT NULL,
cpf VARCHAR2(14) NOT NULL, 
senha VARCHAR2(20) NOT NULL,
peso NUMBER NOT NULL, 
altura NUMBER NOT NULL,
sexo VARCHAR2(6) NOT NULL,
CONSTRAINT usuario_pk PRIMARY KEY(cpf)
);

<!-- sequence para gerar o auto incremento do id de usuario-->

CREATE SEQUENCE usuario_sequence_id;

<!-- trigger que se encarrega de adicionar valor ao id do usuario sempre que for inserido um novo-->

CREATE OR REPLACE TRIGGER usuario_trigger_id 
BEFORE INSERT ON usuario
FOR EACH ROW
BEGIN
  SELECT usuario_sequence_id.NEXTVAL 
	INTO:new.id FROM dual; 
END;