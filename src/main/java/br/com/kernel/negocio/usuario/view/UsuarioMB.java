package br.com.kernel.negocio.usuario.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.kernel.infra.crud.AbstractBean;
import br.com.kernel.negocio.usuario.controller.UsuarioController;
import br.com.kernel.negocio.usuario.model.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioMB extends AbstractBean<Usuario>{
	
	private static final long serialVersionUID = 1L;
	
	public UsuarioMB() {
		super(Usuario.class, new UsuarioController());
		// TODO Auto-generated constructor stub
		usuario = new Usuario();
	}

	private Usuario usuario;
	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public Usuario getInstance() {
		// TODO Auto-generated method stub
		return this.usuario;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		setUsuario(this.getObjetoSelecionado());
	}

	@Override
	public void onRowSelect() {
		// TODO Auto-generated method stub
		setUsuario(this.getObjetoSelecionado());		
	}
	
	
	public String salvarUsuario() {
		// TODO Auto-generated method stub
		super.salvar();
		return "index";
	}
	
	

}
