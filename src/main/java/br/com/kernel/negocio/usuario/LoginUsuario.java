package br.com.kernel.negocio.usuario;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.kernel.infra.banco.dao.DaoImp;
import br.com.kernel.negocio.usuario.model.Usuario;

@ManagedBean(name = "loginUsuario", eager= true)
@SessionScoped
public class LoginUsuario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioSessao;
	private String cpf;
	private String senha;
	
	
	public String logar(){
		DaoImp<Usuario> usuarioDAO = new DaoImp(Usuario.class);
		try {
			this.usuarioSessao = usuarioDAO.obterPorId(cpf);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return null;
		}
		
		if (this.usuarioSessao != null && (this.cpf.equals(this.usuarioSessao.getCpf())) && (this.senha.equals(this.usuarioSessao.getSenha()))) {
			return "homePaciente";
		}
		else{
			return null;
		}
		
	}


	public Usuario getUsuarioSessao() {
		return usuarioSessao;
	}


	public void setUsuarioSessao(Usuario usuarioSessao) {
		this.usuarioSessao = usuarioSessao;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}

}
