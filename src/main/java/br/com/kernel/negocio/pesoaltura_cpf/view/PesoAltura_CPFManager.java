package br.com.kernel.negocio.pesoaltura_cpf.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.kernel.infra.crud.AbstractBean;
import br.com.kernel.negocio.pesoaltura_cpf.controller.PesoAltura_CPFController;
import br.com.kernel.negocio.pesoaltura_cpf.model.PesoAltura_CPF;

@ManagedBean
@ViewScoped
public class PesoAltura_CPFManager extends AbstractBean<PesoAltura_CPF>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PesoAltura_CPF pesoaltura_CPF;

	public PesoAltura_CPFManager(){
		super(PesoAltura_CPF.class, new PesoAltura_CPFController());
		pesoaltura_CPF = new PesoAltura_CPF();
	}

	@Override
	public PesoAltura_CPF getInstance() {
		// TODO Auto-generated method stub
		return this.pesoaltura_CPF;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		setPesoaltura_CPF(this.getObjetoSelecionado());
	}

	@Override
	public void onRowSelect() {
		// TODO Auto-generated method stub
		setPesoaltura_CPF(this.getObjetoSelecionado());
	}

	public PesoAltura_CPF getPesoaltura_CPF() {
		return pesoaltura_CPF;
	}

	public void setPesoaltura_CPF(PesoAltura_CPF pesoaltura_CPF) {
		this.pesoaltura_CPF = pesoaltura_CPF;
	}
	
}
