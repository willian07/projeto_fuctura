package br.com.kernel.negocio.pesoaltura_cpf.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.kernel.negocio.usuario.model.Usuario;

@Entity
@Table(name = "TBL_PES_ALT_CPF")
@SequenceGenerator(name = "SEQ_PES_ALT_CPF", sequenceName = "SEQ_PES_ALT_CPF")
public class PesoAltura_CPF implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private CPFId cpfId;
	
	@ManyToOne
	@JoinColumn(name = "CPF", referencedColumnName = "CPF", nullable = false, updatable = false, insertable = false)
	private Usuario usuario;
	
	@Column(name = "PESO", nullable = false)
	private double peso;
	@Column(name = "ALTURA", nullable = false)
	private double altura;
	@Column(name = "DATA_REGISTRO")
	@Temporal(TemporalType.DATE)
	private Date dataRegistro;
	
			
	public PesoAltura_CPF(CPFId cpfId, Usuario usuario, double peso, double altura, Date dataRegistro) {
		super();
		this.cpfId = cpfId;
		this.usuario = usuario;
		this.peso = peso;
		this.altura = altura;
		this.dataRegistro = dataRegistro;
	}

	public PesoAltura_CPF() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CPFId getCpfId() {
		return cpfId;
	}

	public void setCpfId(CPFId cpfId) {
		this.cpfId = cpfId;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public Date getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(Date dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public double getImc() {
		return (this.peso / (altura * altura));
	}

}
