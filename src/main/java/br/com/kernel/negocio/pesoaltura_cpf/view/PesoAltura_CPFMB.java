package br.com.kernel.negocio.pesoaltura_cpf.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.kernel.infra.crud.AbstractBean;
import br.com.kernel.negocio.pesoaltura_cpf.controller.PesoAltura_CPFController;
import br.com.kernel.negocio.pesoaltura_cpf.model.PesoAltura_CPF;

@ManagedBean
@ViewScoped
public class PesoAltura_CPFMB extends AbstractBean<PesoAltura_CPF>{

	public PesoAltura_CPFMB() {
		super(PesoAltura_CPF.class, new PesoAltura_CPFController());
		// TODO Auto-generated constructor stub
		pesoAltura_CPF = new PesoAltura_CPF();
	}
	
	private static final long serialVersionUID = 1L;
	
	private PesoAltura_CPF pesoAltura_CPF;
	

	public PesoAltura_CPF getPesoAltura_CPF() {
		return pesoAltura_CPF;
	}

	public void setPesoAltura_CPF(PesoAltura_CPF pesoAltura_CPF) {
		this.pesoAltura_CPF = pesoAltura_CPF;
	}

	/**
	 * 
	 */
	
	@Override
	public PesoAltura_CPF getInstance() {
		// TODO Auto-generated method stub
		return this.pesoAltura_CPF;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		setPesoAltura_CPF(this.getObjetoSelecionado());
	}

	@Override
	public void onRowSelect() {
		// TODO Auto-generated method stub
		setPesoAltura_CPF(this.getObjetoSelecionado());
	}

}
