package br.com.kernel.infra.tela;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
 
@ManagedBean
public class ChartView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LineChartModel animatedModel1;
 
    @PostConstruct
    public void init() {
        createAnimatedModels();
    }
 
    public LineChartModel getAnimatedModel1() {
        return animatedModel1;
    }
 
 
    private void createAnimatedModels() {
        animatedModel1 = initLinearModel();
        animatedModel1.setTitle("IMC");
        animatedModel1.setAnimate(true);
        animatedModel1.setLegendPosition("se");
        Axis yAxis = animatedModel1.getAxis(AxisType.Y);
        yAxis.setMin(15);
        yAxis.setMax(35);
        
    }
     
        
    private LineChartModel initLinearModel() {
        LineChartModel model = new LineChartModel();
 
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("NomePaciente");
 
        series1.set(1, 22);
        series1.set(2, 21);
        series1.set(3, 23);
        series1.set(4, 26);
        series1.set(5, 28);
 
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("Normal");
 
        series2.set(1, 21);
        series2.set(2, 22);
        series2.set(3, 22);
        series2.set(4, 23);
        series2.set(5, 22);
 
        model.addSeries(series1);
        model.addSeries(series2);
         
        return model;
    }
    
}
